export default {
  tableFields: [
    {
      name: '__component:badge-column',
      title: '',
      dataClass: 'text-center'
    },
    {
      name: 'Nome Paciente',
      sortField: 'nome'
    },
    {
      name: 'email',
      sortField: 'email'
    },
    {
      name: 'telefone_celular',
      title: 'Celular'
    },
    {
      name: 'cidade',
      title: 'cidade'
    }
  ],
  sortFunctions: {
    'nome': function (item1, item2) {
      return item1 >= item2 ? 1 : -1
    },
    'email': function (item1, item2) {
      return item1 >= item2 ? 1 : -1
    }
  }
}
