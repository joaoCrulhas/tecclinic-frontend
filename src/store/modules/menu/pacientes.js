import lazyLoading from './lazyLoading'

export default {
  name: 'Pacientes',
  meta: {
    expanded: false,
    title: 'Pacientes',
    iconClass: 'fa fa-address-book'
  },
  children: [
    {
      name: 'ListarPacientes',
      path: '/pacientes',
      component: lazyLoading('pacientes/ListaPacientes'),
      meta: {
        title: 'Listar Pacientes'
      }
    },
    {
      name: 'CadastrarPacientes',
      path: '/pacientes/add',
      component: lazyLoading('pacientes/CadastrarPaciente'),
      meta: {
        title: 'Cadastrar Paciente'
      }
    }
  ]
}

